import torch

number_of_players = 2
max_number_of_steps = 150
number_of_episodes = 1000
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
lr = 0.01
replay_memory_capacity = 2000
min_epsilon = 0.1
winner_score = 100
loser_score = -100
initial_exploration = 1000
decreasing_rate_of_epsilon = 0.000005
batch_size = 32
update_target = 100
log_interval = 5
window_size = 10
