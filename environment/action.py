from environment.action_type import ActionType
from environment.color import Color
from environment.jeton import Jeton
import environment.card as CardClass


def Convert_to_Action(s: str):
    broken = s.split(" , Return Tokens: ")
    back_token = (broken.__len__() == 2)
    first_part = broken[0]
    returned_tokens = []
    if back_token:

        tokens_str = broken[1].split(", ")[0:-1]
        for token_str in tokens_str:
            returned_tokens.append(Jeton(Color[token_str]))
    if first_part.split("Pick Tokens: ").__len__() == 2:
        type = ActionType.pick_token
        tokens_str = first_part.split("Pick Tokens: ")[1].split(", ")[0:-1]
        tokens = []
        for token_str in tokens_str:
            tokens.append(Jeton(Color[token_str.split("Color= ")[1]]))
        if back_token:
            return Action(type, back_token, [tokens, returned_tokens])
        return Action(type, back_token, [tokens])
    if first_part.split("Play Card: ").__len__() == 2:
        type = ActionType.play_card
        card = CardClass.Convert_to_Card(first_part.split("Play Card: ")[1])
        return Action(type, False, [card])
    if first_part.split("Reserve Card: ").__len__() == 2:
        type = ActionType.reserve_card
        card = CardClass.Convert_to_Card(first_part.split("Reserve Card: ")[1])
        if back_token:
            return Action(type, back_token, [card, False, returned_tokens])
        return Action(type, back_token, [card, False])
    if first_part.split("Reserve Card with token: ").__len__() == 2:
        type = ActionType.reserve_card
        card = CardClass.Convert_to_Card(first_part.split("Reserve Card with token: ")[1])
        if back_token:
            return Action(type, back_token, [card, True, returned_tokens])
        return Action(type, back_token, [card, True])
    if first_part.split("Reserve Hidden Card with token: ").__len__() == 2:
        type = ActionType.reserve_hidden_card
        color = Color[first_part.split("Reserve Hidden Card with token: ")[1]]
        if back_token:
            return Action(type, back_token, [color, True, returned_tokens])
        return Action(type, back_token, [color, True])
    type = ActionType.reserve_hidden_card
    color = Color[first_part.split("Reserve Hidden Card: ")[1]]
    return Action(type, back_token, [color, False])


class Action:

    def __init__(self, type, back_token, args):
        self.type = type
        self.back_token = back_token
        self.received_tokens = None
        self.returned_tokens = None
        self.card = None
        self.pick_yellow = None
        self.color = None
        if type == ActionType.pick_token:
            self.received_tokens = args[0]
            if back_token:
                self.returned_tokens = args[1]
        if type == ActionType.play_card:
            self.card = args[0]
        if type == ActionType.reserve_card:
            self.card = args[0]
            self.pick_yellow = args[1]
            if back_token:
                self.returned_tokens = args[2]
        if type == ActionType.reserve_hidden_card:
            self.color = args[0]
            self.pick_yellow = args[1]
            if back_token:
                self.returned_tokens = args[2]

    def __str__(self):
        s = ""
        if self.type == ActionType.pick_token:
            s = "Pick Tokens: "
            for token in self.received_tokens:
                s = s + str(token) + ", "
        if self.type == ActionType.play_card:
            s = "Play Card: " + str(self.card)
        if self.type == ActionType.reserve_card:
            if self.pick_yellow:
                s = "Reserve Card with token: " + str(self.card)
            else:
                s = "Reserve Card: " + str(self.card)
        if self.type == ActionType.reserve_hidden_card:
            if self.pick_yellow:
                s = "Reserve Hidden Card with token: " + str(self.color)
            else:
                s = "Reserve Hidden Card: " + str(self.color)
        if self.back_token:
            s += " , Return Tokens: "
            for jeton in self.returned_tokens:
                s += str(jeton.color) + ", "
        return s


    def __eq__(self, other):
        return str(self) == str(other)