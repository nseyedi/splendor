from enum import Enum


class ActionType(Enum):
    pick_token = 0
    play_card = 1
    reserve_card = 2
    reserve_hidden_card = 3