from environment.color import Color


def Convert_to_Card(s: str):
    s = s.split("Number = ")[1].split(", Back Color = ")
    number = int(float(s[0]))
    s = s[1].split(", Color = ")
    backcolor = Color[s[0]]
    s = s[1].split(", Score = ")
    color = Color[s[0]]
    s = s[1].split(", req = ")
    score = int(float(s[0]))
    r = s[1].replace("[", "").replace("]", "").split(", ")
    req = []
    for i in r:
        req.append(int(float(i)))
    return Card(number, backcolor, color, score, req)


class Card:
    def __init__(self, number, backcolor, color, score, req):
        self.number = number
        self.backcolor = backcolor
        self.color = color
        self.score = score
        self.req = req

    def __eq__(self, other):
        return self.color == other.color and self.score == other.score and self.req == other.req

    def __str__(self):
        s = "Number = " + str(self.number) + ", Back Color = " + str(self.backcolor) + ", Color = " + str(
            self.color) + ", Score = " + str(self.score) + ", req = " + str(self.req)
        return s
