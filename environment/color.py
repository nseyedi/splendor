from enum import Enum


class Color(Enum):
    red = 0
    blue = 1
    green = 2
    white = 3
    black = 4
    yellow = 5

    def __str__(self):
        return self.name