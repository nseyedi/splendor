from environment.color import Color
from environment.init import CardExtractor
from environment.jeton import Jeton
from environment.state import State
from environment.action_type import ActionType
from environment.action import Action
import environment.action as actionClass
import random
import numpy as np


class SplendorEnv:
    def __init__(self, n):
        if 4 >= n >= 2:
            self.number_of_players = n
        else:
            raise ValueError("number of players is not correct!")
        self.blue, self.yellow, self.green, self.invest = CardExtractor().run()
        self.current_state: State
        self.total_number_of_cards = len(self.blue) + len(self.yellow) + len(self.green)
        self.total_number_of_invests = len(self.invest)
        self.number_of_showing_cards = 4
        self.number_of_stacks = 3
        self.action_space, self.inv_action_space = self.get_all_actions()

    def reset(self):
        blue_index = list(range(self.blue.__len__()))
        yellow_index = list(range(self.yellow.__len__()))
        green_index = list(range(self.green.__len__()))
        invest_index = list(range(self.invest.__len__()))

        random.shuffle(blue_index)
        random.shuffle(yellow_index)
        random.shuffle(green_index)
        random.shuffle(invest_index)

        for i in range(len(blue_index)):
            blue_index[i] = self.blue[blue_index[i]]
        for i in range(len(yellow_index)):
            yellow_index[i] = self.yellow[yellow_index[i]]
        for i in range(len(green_index)):
            green_index[i] = self.green[green_index[i]]

        final_invest = invest_index[0:(self.number_of_players + 1)]
        for i in range(len(final_invest)):
            final_invest[i] = self.invest[final_invest[i]]
        stack = [blue_index, yellow_index, green_index]
        cards = []
        tokens = []
        visible_reserves = []
        hidden_reserves = []
        invests = []
        for i in range(self.number_of_players):
            cards.append([])
            tokens.append(np.zeros(Color.__len__()))
            visible_reserves.append([])
            hidden_reserves.append([])
            invests.append([])

        available_tokens = np.zeros(Color.__len__())
        for i in range(Color.__len__() - 1):
            available_tokens[i] = self.number_of_players + 3
        available_tokens[Color.__len__() - 1] = 5

        self.current_state = State(number_of_players=self.number_of_players,
                                   available_invests=final_invest,
                                   available_tokens=available_tokens,
                                   player_cards=cards,
                                   player_tokens=tokens,
                                   player_hidden_reserve=hidden_reserves,
                                   player_visible_reserves=visible_reserves,
                                   player_invests=invests, stacks=stack)

    def get_partial_observation(self, player):
        partial = []
        itself = np.zeros(self.number_of_players)
        itself[player] = 1
        partial += itself.tolist()
        head_cards = np.zeros(self.total_number_of_cards)
        stacks = self.current_state.stacks
        for i in range(self.number_of_stacks):
            max_ind = min(self.number_of_showing_cards, len(stacks[i]))
            for card in stacks[i][0:max_ind]:
                head_cards[card.number] += 1
        head_cards = head_cards.tolist()
        partial += head_cards
        head_invests = np.zeros(self.total_number_of_invests)
        invests = self.current_state.available_invests
        for card in invests:
            head_invests[card.number] += 1
        head_invests = head_invests.tolist()
        partial += head_invests
        player_cards = []
        for p in range(self.number_of_players):
            cards = np.zeros(self.total_number_of_cards)
            for card in self.current_state.player_cards[p]:
                cards[card.number] += 1
            player_cards += cards.tolist()
        partial += player_cards
        visible = []
        for p in range(self.number_of_players):
            cards = np.zeros(self.total_number_of_cards)
            for card in self.current_state.player_visible_reserves[p]:
                cards[card.number] += 1
            visible += cards.tolist()
        partial += visible
        ithiddn = np.zeros(self.total_number_of_cards)
        for card in self.current_state.get_hidden_reserves(player):
            ithiddn[card.number] += 1
        partial += ithiddn.tolist()
        hiddens = []
        for p in range(self.number_of_players):
            if p == player: continue
            hiddens += self.current_state.get_hiddens_based_on_backcolor(p).tolist()
        partial += hiddens
        tokens = []
        for p in range(self.number_of_players):
            tokens += self.current_state.get_tokens(p).tolist()
        partial += tokens
        player_inv = []
        for p in range(self.number_of_players):
            cards = np.zeros(self.total_number_of_invests)
            for card in self.current_state.player_invests[p]:
                cards[card.number] += 1
            player_inv += cards.tolist()
        partial += player_inv
        partial += self.current_state.available_tokens.tolist()
        partial += self.get_valid_action_space(player)

        return partial

    def get_state_space(self):
        n = self.number_of_players
        ret = n
        ret += self.total_number_of_cards * (2 * n + 2)
        ret += self.number_of_stacks * (n - 1)
        ret += self.total_number_of_invests * (n + 1)
        ret += Color.__len__() * (n + 1)
        ret += self.get_action_space()
        return ret

    def get_action_space(self):
        return len(self.action_space)

    def get_valid_action_space(self, player):
        actions = self.current_state.get_action_space(player)
        ret = np.zeros(self.get_action_space())
        for action in actions:
            ret[self.action_space[str(action)]] = 1
        return ret.tolist()

    def step(self, player, action_index):
        actions = self.get_valid_action_space(player)
        if actions[action_index] == 0:
            raise ValueError("not acceptable action!")
        act = actionClass.Convert_to_Action(self.inv_action_space[action_index])
        reward = 0
        if act.type == ActionType.pick_token:
            self.current_state.pick_token(player, act)
        if act.type == ActionType.play_card:
            reward = self.current_state.play_card(player, act)
        if act.type == ActionType.reserve_card:
            self.current_state.reserve_visible_card(player, act)
        if act.type == ActionType.reserve_hidden_card:
            self.current_state.reserve_hidden_card(player, act)
        return reward

    def get_score(self, player):
        return self.current_state.get_player_score(player)

    def get_winner(self):
        if not self.is_done(): return -1
        winner = 0
        for player in range(self.number_of_players):
            if self.get_score(winner) < self.get_score(player):
                winner = player
            else:
                if self.get_score(winner) == self.get_score(player):
                    if self.current_state.get_number_of_played_cards(winner) > self.current_state.get_number_of_played_cards(player):
                        winner = player
        return winner

    def is_done(self):
        return self.current_state.is_done()

    def set_seed(self, seed):
        random.seed(seed)

    def get_action_score(self, action_ind):
        action = actionClass.Convert_to_Action(self.inv_action_space[action_ind])
        if action.type == ActionType.play_card:
            return action.card.score
        return 0

    def get_all_actions(self):
        dict = {}
        n = Color.__len__() - 1
        available_per = [[]]
        for i in range(n):
            available_per.append([Jeton(Color(i))])
            available_per.append([Jeton(Color(i)), Jeton(Color(i))])
            for j in range(i + 1, n):
                available_per.append([Jeton(Color(i)), Jeton(Color(j))])
                for k in range(j + 1, n):
                    available_per.append([Jeton(Color(i)), Jeton(Color(j)), Jeton(Color(k))])
        for x in available_per:
            act = Action(ActionType.pick_token, False, [x])
            dict[str(act)] = dict.__len__()
            if x.__len__() >= 1:
                for i in range(n + 1):
                    y = [Jeton(Color(i))]
                    act = Action(ActionType.pick_token, True, [x, y])
                    dict[str(act)] = dict.__len__()
            if x.__len__() >= 2:
                for i in range(n + 1):
                    for j in range(i, n + 1):
                        y = [Jeton(Color(i)), Jeton(Color(j))]
                        act = Action(ActionType.pick_token, True, [x, y])
                        dict[str(act)] = dict.__len__()
            if x.__len__() >= 3:
                for i in range(n + 1):
                    for j in range(i, n + 1):
                        for k in range(j, n + 1):
                            y = [Jeton(Color(i)), Jeton(Color(j)), Jeton(Color(k))]
                            act = Action(ActionType.pick_token, True, [x, y])
                            dict[str(act)] = dict.__len__()

        total_cards = self.green + self.yellow + self.blue
        for card in total_cards:
            act = Action(ActionType.play_card, False, [card])
            dict[str(act)] = dict.__len__()
            act = Action(ActionType.reserve_card, False, [card, False])
            dict[str(act)] = dict.__len__()
            act = Action(ActionType.reserve_card, False, [card, True])
            dict[str(act)] = dict.__len__()
            for i in range(n + 1):
                act = Action(ActionType.reserve_card, True, [card, True, [Jeton(Color(i))]])
                dict[str(act)] = dict.__len__()
        act = Action(ActionType.reserve_hidden_card, False, [Color.green, False])
        dict[str(act)] = dict.__len__()
        act = Action(ActionType.reserve_hidden_card, False, [Color.green, True])
        dict[str(act)] = dict.__len__()
        act = Action(ActionType.reserve_hidden_card, False, [Color.yellow, False])
        dict[str(act)] = dict.__len__()
        act = Action(ActionType.reserve_hidden_card, False, [Color.yellow, True])
        dict[str(act)] = dict.__len__()
        act = Action(ActionType.reserve_hidden_card, False, [Color.blue, False])
        dict[str(act)] = dict.__len__()
        act = Action(ActionType.reserve_hidden_card, False, [Color.blue, True])
        dict[str(act)] = dict.__len__()
        for i in range(n + 1):
            act = Action(ActionType.reserve_hidden_card, True, [Color.green, True, [Jeton(Color(i))]])
            dict[str(act)] = dict.__len__()
            act = Action(ActionType.reserve_hidden_card, True, [Color.yellow, True, [Jeton(Color(i))]])
            dict[str(act)] = dict.__len__()
            act = Action(ActionType.reserve_hidden_card, True, [Color.blue, True, [Jeton(Color(i))]])
            dict[str(act)] = dict.__len__()

        inv_act = {v: k for k, v in dict.items()}
        return dict, inv_act
