from environment.card import Card
from environment.color import Color
from environment.investment import Investment
import pandas as pd
import os


def extract_invest(init_number, path):
    ret = []
    df = pd.read_csv(path)
    number = init_number
    for i in range(df.shape[0]):
        ret.append(Investment(number, [df.at[i, "Red"], df.at[i, "Blue"], df.at[i, "Green"], df.at[i, "White"],
                                       df.at[i, "Black"]]))
        number += 1
    return ret, number


def extract_cards(init_number, back, path):
    ret = []
    number = init_number
    df = pd.read_csv(path)
    for i in range(df.shape[0]):
        ret.append(Card(number, back, Color(df.at[i, "Color"]), df.at[i, "Score"],
                        [df.at[i, "Red"], df.at[i, "Blue"], df.at[i, "Green"], df.at[i, "White"],
                         df.at[i, "Black"]]))
        number += 1
    return ret, number


class CardExtractor:
    blue_path = "../data/blue.csv"
    yellow_path = "../data/yellow.csv"
    green_path = "../data/green.csv"
    invest_path = "../data/invest.csv"
    if os.getcwd()[-7:-1] == 'Projec':
        blue_path = "data/blue.csv"
        yellow_path = "data/yellow.csv"
        green_path = "data/green.csv"
        invest_path = "data/invest.csv"


    def __init__(self):
        self.invest, num = extract_invest(0, self.invest_path)
        self.blue, num = extract_cards(num, Color.blue, self.blue_path)
        self.yellow, num = extract_cards(num, Color.yellow, self.yellow_path)
        self.green, num = extract_cards(0, Color.green, self.green_path)

    def run(self):
        return self.blue, self.yellow, self.green, self.invest
