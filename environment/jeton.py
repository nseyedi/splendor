class Jeton:
    def __init__(self, color):
        self.color = color

    def __eq__(self, other):
        return self.color == other.color

    def __str__(self):
        return "Color= " + str(self.color)
