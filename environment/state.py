from environment.action import Action
from environment.action_type import ActionType
from environment.color import Color
import numpy as np
from environment.jeton import Jeton


class State:
    player_hidden_reserve: list

    def __init__(self, number_of_players, stacks, player_cards, player_visible_reserves
                 , player_hidden_reserve, player_tokens, available_invests, player_invests, available_tokens):
        self.number_of_players = number_of_players
        self.stacks = stacks
        self.player_cards = player_cards
        self.player_visible_reserves = player_visible_reserves
        self.player_hidden_reserve = player_hidden_reserve
        self.player_tokens = player_tokens
        self.available_invests = available_invests
        self.player_invests = player_invests
        self.available_tokens = available_tokens

    def get_tokens(self, player):
        return self.player_tokens[player]

    def get_cards(self, player):
        return self.player_cards[player]

    def get_number_of_invest(self, player):
        return len(self.player_invests[player])

    def get_visible_reserves(self, player):
        return self.player_visible_reserves[player]

    def get_hidden_reserves(self, player):
        return self.player_hidden_reserve[player]

    def get_number_of_cards_based_on_color(self, player):
        cards = self.get_cards(player)
        ret = np.zeros(Color.__len__())
        for card in cards:
            ret[card.color.value] += 1
        return ret

    def get_hiddens_based_on_backcolor(self, player):
        cards = self.get_hidden_reserves(player)
        ret = np.zeros(3)
        for card in cards:
            if card.backcolor == Color.green:
                ret[0] += 1
            if card.backcolor == Color.yellow:
                ret[1] += 1
            if card.backcolor == Color.blue:
                ret[2] += 1
        return ret

    def total_number_of_colors_for_player(self, player):
        return self.get_tokens(player) + self.get_number_of_cards_based_on_color(player)

    def is_possible_to_play_card(self, player, card):
        req = card.req
        diff = 0
        total = self.total_number_of_colors_for_player(player)
        for i in range(Color.__len__() - 1):
            diff += max(0, req[i] - total[i])

        return diff <= total[Color.yellow.value]

    def get_number_of_reserves(self, player):
        return len(self.player_hidden_reserve[player]) + len(self.player_visible_reserves[player])

    def is_possible_to_reserve_card(self, player):
        return self.get_number_of_reserves(player) < 3

    def is_possible_to_pick_invest(self, player, investment):
        req = investment.req
        cardsColor = self.get_number_of_cards_based_on_color(player)
        ret = req <= cardsColor[0:-1]
        return ret.min()

    def pick_invest(self, player):
        packet = []
        for invest in self.available_invests:
            if self.is_possible_to_pick_invest(player, invest):
                packet.append(invest)

        for invest in packet:
            self.available_invests.remove(invest)
            self.player_invests[player].append(invest)

    def get_player_score(self, player):
        cards = self.get_cards(player)
        score = 0
        for card in cards:
            score = score + card.score
        score = score + self.get_number_of_invest(player) * 3
        return score

    def is_done(self):
        for i in range(self.number_of_players):
            score = self.get_player_score(i)
            if score >= 15:
                return True
        return False

    def get_head_of_cards(self):
        n = 4
        ret = []
        for t in range(3):
            ret.append([])
            max_ind = min(n, len(self.stacks[t]))
            for card in self.stacks[t][0:max_ind]:
                ret[t].append(card)
        return ret

    def get_vectorized_head_of_cards(self):
        head = self.get_head_of_cards()
        ret = head[0] + head[1] + head[2]
        return ret

    def get_pick_token_actions(self, player):
        player_tokens = self.get_tokens(player)
        m = sum(player_tokens)
        total_tokens = self.available_tokens
        available_per = [[]]
        ret = []
        n = Color.__len__() - 1
        for i in range(n):
            if total_tokens[i] == 0: continue
            available_per.append([Jeton(Color(i))])
            if total_tokens[i] > 3:
                available_per.append([Jeton(Color(i)), Jeton(Color(i))])
            for j in range(i + 1, n):
                if total_tokens[j] == 0: continue
                available_per.append([Jeton(Color(i)), Jeton(Color(j))])
                for k in range(j + 1, n):
                    if total_tokens[k] == 0: continue
                    available_per.append([Jeton(Color(i)), Jeton(Color(j)), Jeton(Color(k))])
        for x in available_per:
            sz = m + x.__len__()
            if sz <= 10:
                ret.append(Action(ActionType.pick_token, False, [x]))
            if sz == 11:
                for i in range(n):
                    if player_tokens[i] == 0: continue
                    ret.append(Action(ActionType.pick_token, True, [x, [Jeton(Color(i))]]))
            if sz == 12:
                for i in range(n):
                    if player_tokens[i] == 0: continue
                    if player_tokens[i] >= 2:
                        ret.append(Action(ActionType.pick_token, True, [x, [Jeton(Color(i)), Jeton(Color(i))]]))
                    for j in range(i + 1, n):
                        if player_tokens[j] == 0: continue
                        ret.append(Action(ActionType.pick_token, True, [x, [Jeton(Color(i)), Jeton(Color(j))]]))
            if sz == 13:
                for i in range(n):
                    if player_tokens[i] == 0: continue
                    if player_tokens[i] >= 3:
                        y = [Jeton(Color(i)), Jeton(Color(i)), Jeton(Color(i))]
                        ret.append(Action(ActionType.pick_token, True, [x, y]))
                    if player_tokens[i] >= 2:
                        for j in range(i + 1, n):
                            if player_tokens[j] == 0: continue
                            y = [Jeton(Color(i)), Jeton(Color(i)), Jeton(Color(j))]
                            ret.append(Action(ActionType.pick_token, True, [x, y]))
                    for j in range(i + 1, n):
                        if player_tokens[j] == 0: continue
                        if player_tokens[j] >= 2:
                            y = [Jeton(Color(i)), Jeton(Color(j)), Jeton(Color(j))]
                            ret.append(Action(ActionType.pick_token, True, [x, y]))
                        for k in range(j + 1, n):
                            if player_tokens[k] == 0: continue
                            y = [Jeton(Color(i)), Jeton(Color(j)), Jeton(Color(k))]
                            ret.append(Action(ActionType.pick_token, True, [x, y]))
        return ret

    def get_play_card_actions(self, player):
        ret = []
        total = self.get_visible_reserves(player)
        total = total + self.get_hidden_reserves(player)
        total = total + self.get_vectorized_head_of_cards()
        for card in total:
            if self.is_possible_to_play_card(player, card):
                ret.append(Action(ActionType.play_card, False, [card]))
        return ret

    def get_reserve_card_actions(self, player):
        if self.is_possible_to_reserve_card(player) == False: return []
        take_yellow = False
        if self.available_tokens[Color.yellow.value] > 0: take_yellow = True
        tokens = self.get_tokens(player)
        number_of_tokens = sum(tokens)
        warning = False
        if number_of_tokens == 10 and take_yellow:
            warning = True
        ret = []
        head = self.get_vectorized_head_of_cards()
        for card in head:
            if warning:
                for i in range(Color.__len__()):
                    if tokens[i] == 0: continue
                    ret.append(Action(ActionType.reserve_card, warning, [card, take_yellow, [Jeton(Color(i))]]))
            else:
                ret.append(Action(ActionType.reserve_card, warning, [card, take_yellow]))
        if len(self.stacks[0]) > 4:
            if warning:
                for i in range(Color.__len__()):
                    if tokens[i] == 0: continue
                    ret.append(
                        Action(ActionType.reserve_hidden_card, warning, [Color.blue, take_yellow, [Jeton(Color(i))]]))
            else:
                ret.append(Action(ActionType.reserve_hidden_card, warning, [Color.blue, take_yellow]))
        if len(self.stacks[1]) > 4:
            if warning:
                for i in range(Color.__len__()):
                    if tokens[i] == 0: continue
                    ret.append(
                        Action(ActionType.reserve_hidden_card, warning, [Color.yellow, take_yellow, [Jeton(Color(i))]]))
            else:
                ret.append(Action(ActionType.reserve_hidden_card, warning, [Color.yellow, take_yellow]))
        if len(self.stacks[2]) > 4:
            if warning:
                for i in range(Color.__len__()):
                    if tokens[i] == 0: continue
                    ret.append(
                        Action(ActionType.reserve_hidden_card, warning, [Color.green, take_yellow, [Jeton(Color(i))]]))
            else:
                ret.append(Action(ActionType.reserve_hidden_card, warning, [Color.green, take_yellow]))
        return ret

    def get_action_space(self, player):
        action_space = self.get_pick_token_actions(player)
        action_space = action_space + self.get_play_card_actions(player)
        action_space = action_space + self.get_reserve_card_actions(player)
        return action_space

    def pick_token(self, player, action):
        for jeton in action.received_tokens:
            self.player_tokens[player][jeton.color.value] += 1
            self.available_tokens[jeton.color.value] -= 1
        if action.back_token:
            for jeton in action.returned_tokens:
                self.player_tokens[player][jeton.color.value] -= 1
                self.available_tokens[jeton.color.value] += 1

    def play_card(self, player, action):
        card = action.card
        req = card.req
        diff = 0
        total = self.total_number_of_colors_for_player(player)
        for i in range(Color.__len__() - 1):
            diff += max(0, req[i] - total[i])
            self.player_tokens[player][i] -= min(self.player_tokens[player][i], req[i])
            self.available_tokens[i] += min(self.player_tokens[player][i], req[i])

        self.player_tokens[player][Color.yellow.value] -= diff
        self.available_tokens[Color.yellow.value] += diff

        if card in self.player_visible_reserves[player]:
            self.player_visible_reserves[player].remove(card)
        else:
            if card in self.player_hidden_reserve[player]:
                self.player_hidden_reserve[player].remove(card)
            else:
                if card.backcolor == Color.blue:
                    self.stacks[0].remove(card)
                if card.backcolor == Color.yellow:
                    self.stacks[1].remove(card)
                if card.backcolor == Color.green:
                    self.stacks[2].remove(card)
        self.player_cards[player].append(card)
        self.pick_invest(player)
        return card.score

    def reserve_visible_card(self, player, action):
        card = action.card
        if card.backcolor == Color.blue:
            self.stacks[0].remove(card)
        if card.backcolor == Color.yellow:
            self.stacks[1].remove(card)
        if card.backcolor == Color.green:
            self.stacks[2].remove(card)
        self.player_visible_reserves[player].append(card)
        self.manage_reservation_token(player, action)

    def reserve_hidden_card(self, player, action):
        card = None
        n = 4
        if action.color == Color.blue:
            card = self.stacks[0][n]
            self.stacks[0].remove(card)
        if action.color == Color.yellow:
            card = self.stacks[1][n]
            self.stacks[1].remove(card)
        if action.color == Color.green:
            card = self.stacks[2][n]
            self.stacks[2].remove(card)
        self.player_hidden_reserve[player].append(card)
        self.manage_reservation_token(player, action)

    def manage_reservation_token(self, player, action):
        if action.pick_yellow:
            self.player_tokens[player][Color.yellow.value] += 1
            self.available_tokens[Color.yellow.value] -= 1
        if action.back_token:
            back = action.returned_tokens[0]
            self.player_tokens[player][back.color.value] -= 1
            self.available_tokens[back.color.value] += 1

    def get_number_of_played_cards(self, player):
        return len(self.player_cards[player])
