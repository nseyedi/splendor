import random
from collections import deque, namedtuple

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Categorical

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'mask'))


class Actor(nn.Module):
    def __init__(self, num_inputs, num_outputs):
        super(Actor, self).__init__()

        self.num_inputs = num_inputs
        self.num_outputs = num_outputs

        self.fc1 = nn.Linear(num_inputs, 128)
        self.fc2 = nn.Linear(128, num_outputs)

        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.xavier_uniform(m.weight)

    def forward(self, x, valid_actions):
        x = x.view(-1, self.num_inputs)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        exp = torch.exp(x)
        cvt = exp * valid_actions
        x = cvt / torch.sum(cvt)
        policy = Categorical(x)
        return policy

    def get_action(self, state, valid_actions):
        state = torch.FloatTensor(state)
        valid_actions = torch.FloatTensor(valid_actions)
        policy = self.forward(state, valid_actions)
        return policy.sample().item()


class Critic(nn.Module):
    def __init__(self, num_inputs, num_outputs):
        super(Critic, self).__init__()

        self.num_inputs = num_inputs
        self.num_outputs = num_outputs

        self.fc1 = nn.Linear(num_inputs, 128)
        self.fc2 = nn.Linear(128, num_outputs)

        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.xavier_uniform(m.weight)

    def forward(self, x):

        x = x.view(-1, self.num_inputs)
        x = F.relu(self.fc1(x))
        qvalue = self.fc2(x)
        return qvalue


class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        """Save a transition"""
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        transitions = random.sample(self.memory, batch_size)
        batch = Transition(*zip(*transitions))
        return batch

    def __len__(self):
        return len(self.memory)
