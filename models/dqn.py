from collections import namedtuple, deque

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from config import *
import random

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward', 'mask'))


def update_target_model(online_net, target_net):
    # Target <- Net
    target_net.load_state_dict(online_net.state_dict())


class DqnAgent:
    def __init__(self, num_inputs, num_actions):
        self.online_net = QNet(num_inputs, num_actions)
        self.target_net = QNet(num_inputs, num_actions)
        update_target_model(self.online_net, self.target_net)

        self.optimizer = optim.Adam(self.online_net.parameters(), lr=lr)

        self.online_net.to(device)
        self.target_net.to(device)
        self.online_net.train()
        self.target_net.train()

        self.memory = ReplayMemory(replay_memory_capacity)

        self.step_counter = 0
        self.epsilon = 1.0
        self.score_list = []

    def get_action(self, state, valid_actions):
        if np.random.rand() <= self.epsilon:
            idx_nonzero, = np.nonzero(valid_actions)
            return np.random.choice(idx_nonzero)
        state = torch.FloatTensor(state)
        valid_actions = torch.FloatTensor(valid_actions)
        qvalue = self.online_net.forward(state)
        qvalue = ((qvalue - qvalue.min()) + 1) * valid_actions
        return qvalue.argmax().item()

    def feed(self, *args):
        self.memory.push(*args)
        self.step_counter += 1
        if self.step_counter > initial_exploration:
            self.epsilon -= decreasing_rate_of_epsilon
            self.epsilon = max(self.epsilon, min_epsilon)

            batch = self.memory.sample(batch_size)
            QNet.train_model(self.online_net, self.target_net, self.optimizer, batch)

            if self.step_counter % update_target:
                update_target_model(self.online_net, self.target_net)
        if args[4]:
            self.score_list.append(args[3])


class QNet(nn.Module):
    def __init__(self, num_inputs, num_outputs):
        super(QNet, self).__init__()

        self.num_inputs = num_inputs
        self.num_outputs = num_outputs

        self.fc1 = nn.Linear(num_inputs, 128)
        self.fc2 = nn.Linear(128, num_outputs)

        for m in self.modules():
            if isinstance(m, nn.Linear):
                nn.init.xavier_uniform(m.weight)

    def forward(self, x):

        x = x.view(-1, self.num_inputs)
        x = F.relu(self.fc1(x))
        qvalue = self.fc2(x)
        return qvalue

    @classmethod
    def train_model(cls, online_net, target_net, optimizer, batch):
        states = torch.tensor(batch.state)
        next_states = torch.tensor(batch.next_state)
        actions = torch.Tensor(batch.action).float().view(-1, 1)
        rewards = torch.Tensor(batch.reward)
        masks = torch.Tensor(batch.mask)

        pred = online_net(states)
        next_pred = target_net(next_states)
        pred = torch.sum(pred.mul(actions))
        target = rewards + masks * next_pred.max(1)[0]

        loss = F.mse_loss(pred, target.detach())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        return loss


class ReplayMemory(object):

    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        """Save a transition"""
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        transitions = random.sample(self.memory, batch_size)
        batch = Transition(*zip(*transitions))
        return batch

    def __len__(self):
        return len(self.memory)
