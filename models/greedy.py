import random
import numpy as np
from environment.env import SplendorEnv


class GreedyAgent:
    def __init__(self, env: SplendorEnv):
        n = env.get_action_space()
        self.scores = np.zeros(n)
        for i in range(n):
            self.scores[i] = env.get_action_score(i)

    def get_action(self, state, valid_actions):
        acts = valid_actions
        act_ind = []
        for ind, i in enumerate(acts):
            if i != 0:
                act_ind.append(ind)
        selected = 0
        for i in act_ind:
            if self.scores[i] > self.scores[selected]:
                selected = i
        if self.scores[selected] != 0:
            return selected
        return random.sample(act_ind, 1)[0]