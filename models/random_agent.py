import random
import numpy as np


class RandomAgent:
    def get_action(self, state, valid_actions):
        idx_nonzero, = np.nonzero(valid_actions)
        return np.random.choice(idx_nonzero)
