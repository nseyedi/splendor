import warnings
from models.dqn import DqnAgent
from environment.env import SplendorEnv
from config import *
from utilities.show_plot import show_plot, show_2_plot
from models.random_agent import RandomAgent
from utilities.save_and_load import load

def main():
    splendor = SplendorEnv(number_of_players)
    state_space = splendor.get_state_space()
    action_space = splendor.get_action_space()

    print("State space: ", state_space)
    print("Action space: ", action_space)

    agents = []
    number_of_wins = [0] * number_of_players
    y = []

    dqn = DqnAgent(state_space, action_space)
    # dqn.target_net = load("../trained_agents/dqn_agent0")
    # dqn.online_net = load("../trained_agents/dqn_agent0")
    dqn.target_net = load("../trained_agents/dqn_decentralized_0")
    dqn.online_net = load("../trained_agents/dqn_decentralized_0")
    agents.append(dqn)
    y.append([])
    for player in range(number_of_players - 1):
        agents.append(RandomAgent())
        y.append([])

    for eps in range(number_of_episodes):
        splendor.reset()
        state = [None] * number_of_players
        action = [None] * number_of_players
        next_state = [None] * number_of_players
        winner = -1
        for step in range(max_number_of_steps):
            for player in range(number_of_players):
                current_state = splendor.get_partial_observation(player)
                valid_actions = splendor.get_valid_action_space(player)
                act_ind = agents[player].get_action(current_state, valid_actions)
                splendor.step(player, act_ind)
                state[player] = current_state
                action[player] = act_ind
                next_state[player] = splendor.get_partial_observation(player)
            if splendor.is_done():
                winner = splendor.get_winner()
                reward = [loser_score] * number_of_players
                reward[winner] = winner_score
                number_of_wins[winner] += 1

            if splendor.is_done():
                break

        for player in range(number_of_players):
            if splendor.is_done():
                if player == winner:
                    y[player].append(1)
                else:
                    y[player].append(-1)
            else:
                y[player].append(0)

        if eps % log_interval == 0:
            print(number_of_wins)

    show_plot(y[0], "DQN")

if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    main()  # DQN vs. Random, decentralized