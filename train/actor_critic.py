import warnings
from models.coma import Actor, Critic
from environment.env import SplendorEnv
from config import *
from utilities.save_and_load import save
from utilities.show_plot import show_plot


def main():
    splendor = SplendorEnv(number_of_players)
    state_space = splendor.get_state_space()
    action_space = splendor.get_action_space()

    print("State space: ", state_space)
    print("Action space: ", action_space)

    agents = []
    number_of_wins = [0] * number_of_players
    y = []
    for player in range(number_of_players):
        agents.append(Actor(state_space, action_space))
        y.append([])
    critic = Critic(state_space, action_space)

    for eps in range(number_of_episodes):
        splendor.reset()
        state = [None] * number_of_players
        action = [None] * number_of_players
        reward = [0] * number_of_players
        next_state = [None] * number_of_players
        mask = [False] * number_of_players
        winner = -1
        for step in range(max_number_of_steps):
            for player in range(number_of_players):
                current_state = splendor.get_partial_observation(player)
                valid_actions = splendor.get_valid_action_space(player)
                act_ind = agents[player].get_action(current_state, valid_actions)
                reward[player] = splendor.step(player, act_ind)
                state[player] = current_state
                action[player] = act_ind
                next_state[player] = splendor.get_partial_observation(player)
            if splendor.is_done():
                winner = splendor.get_winner()
                reward = [loser_score] * number_of_players
                reward[winner] = winner_score
                number_of_wins[winner] += 1
                mask = [True] * number_of_players
            else:
                if step == max_number_of_steps - 1:
                    reward = [loser_score] * number_of_players
                    mask = [True] * number_of_players

            for player in range(number_of_players):
                agents[player].feed(state[player], action[player], next_state[player], reward[player], mask[player])

            if splendor.is_done():
                break

        for player in range(number_of_players):
            if player == winner:
                y[player].append(winner_score)
            else:
                y[player].append(loser_score)

        if eps % log_interval == 0:
            print(eps, ": ", number_of_wins)

    for player in range(number_of_players):
        show_plot(y[player], str(player))

    return agents


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    agents = main()  # DQN vs. DQN, decentralized
    for i, agent in enumerate(agents):
        save(agent.target_net, ("../trained_agents/dqn_decentralized_"+str(i)))