import torch


def save(net, path):
    path = path + ".pt"
    torch.save(net, path)

def load(path):
    path = path + ".pt"
    return torch.load(path)