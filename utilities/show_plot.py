import matplotlib.pyplot as plt
import numpy as np

from config import window_size


def show_2_plot(y1, y2, l1, l2):
    plt.figure(2)
    plt.clf()
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('reward')
    moving_averages = []
    i = 0
    while i < len(y1) - window_size + 1:
        this_window = y1[i: i + window_size]
        window_average = sum(this_window) / window_size
        moving_averages.append(window_average)
        i += 1
    Ep_arr = np.array(moving_averages)
    plt.plot(Ep_arr, label=l1)
    moving_averages = []
    i = 0
    while i < len(y2) - window_size + 1:
        this_window = y2[i: i + window_size]
        window_average = sum(this_window) / window_size
        moving_averages.append(window_average)
        i += 1
    Ep_arr = np.array(moving_averages)
    plt.plot(Ep_arr, label=l2)
    plt.legend()
    plt.show()


def show_plot(y, l1):
    plt.figure(2)
    plt.clf()
    plt.title('Training...')
    plt.xlabel('Episode')
    plt.ylabel('reward')
    moving_averages = []
    i = 0
    while i < len(y) - window_size + 1:
        this_window = y[i: i + window_size]
        window_average = sum(this_window) / window_size
        moving_averages.append(window_average)
        i += 1
    Ep_arr = np.array(moving_averages)
    plt.plot(Ep_arr, label=l1)
    plt.legend()
    plt.show()
